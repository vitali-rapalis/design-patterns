package com.vrapalis.www.dp.sp.decorator;

public class OrderDecorator extends DecoratorWrapper {

    public OrderDecorator(IDecorator decorator) {
        super(decorator);
    }

    @Override
    public void decoratorOperation() {
        decorator.decoratorOperation();
        System.out.println("Order decoration perform operation");
    }
}
