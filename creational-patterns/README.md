# Creational Patterns

---

> **Creational patterns** focus on object creation mechanisms, providing ways to create objects in a flexible and reusable manner. They abstract the process of object instantiation and decouple the code from the specific classes being instantiated. Some common creational patterns include:
