package com.vrapalis.www.dp.sp.composite;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public abstract class AbstractFile implements IFileComponent {
    protected String name;
    protected int fileSize;
}
