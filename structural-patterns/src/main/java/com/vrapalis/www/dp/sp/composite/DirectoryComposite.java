package com.vrapalis.www.dp.sp.composite;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * Participant => Composite
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DirectoryComposite extends AbstractFile {

    private List<IFileComponent> files;

    public DirectoryComposite(String name, int size, List<IFileComponent> files) {
        super(name, size);
        this.files = files;
    }

    @Override
    public String display() {
        return """
                
                Name: %s Size: %d
                ---
                %s
                """.formatted(name, fileSize, files.stream().map(IFileComponent::display).reduce("", (sum, next) -> sum += next));
    }

    @Override
    public int getSize() {
        return files.stream().map(IFileComponent::getSize).reduce(this.fileSize, (sum, next) -> sum += next);
    }
}
