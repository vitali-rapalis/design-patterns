package com.vrapalis.www.dp.bp.strategy;

public interface ISendService {
    void send(SendSubject sendSubject);

    boolean support(Class<? extends SendSubject> cl);

    default void print(String kind, SendSubject sendSubject) {
        System.out.println("""
                Sending %s:
                ---
                To: %s
                Title: %s
                Text: %s
                """.formatted(kind, sendSubject.getParticipant(), sendSubject.getTitle(), sendSubject.getText()));
    }
}
