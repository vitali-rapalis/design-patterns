package com.vrapalis.www.dp.bp.strategy;

public class EmailService implements ISendService {

    @Override
    public void send(SendSubject sendSubject) {
        if (!support(sendSubject.getClass())) {
            throw new UnsupportedOperationException("Only email subjects are allowed");
        }

        print("Email", sendSubject);
    }

    @Override
    public boolean support(Class<? extends SendSubject> cl) {
        return cl == Email.class;
    }
}
