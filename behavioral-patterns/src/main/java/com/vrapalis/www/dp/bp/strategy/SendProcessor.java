package com.vrapalis.www.dp.bp.strategy;

import java.util.List;

public class SendProcessor {
    private List<ISendService> sender = List.of(new EmailService(), new NotificationService(), new SmsService());

    public void process(SendSubject sendSubject) {
        sender.stream()
                .filter(iSendService -> iSendService.support(sendSubject.getClass()))
                .forEach(iSendService -> iSendService.send(sendSubject));
    }

    public void process(List<SendSubject> sendSubjects) {
        sendSubjects.forEach(sendSubject -> process(sendSubject));
    }
}
