package com.vrapalis.www.dp.sp.decorator;

public interface IDecorator {

    void decoratorOperation();
}
