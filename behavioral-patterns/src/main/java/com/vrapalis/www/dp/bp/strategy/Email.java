package com.vrapalis.www.dp.bp.strategy;

public class Email extends SendSubject {
    public Email(String participant, String title, String text) {
        super(participant, title, text);
    }
}
