package com.vrapalis.www.dp.sp.decorator;

public class ShipDecoration extends DecoratorWrapper {

    public ShipDecoration(IDecorator decorator) {
        super(decorator);
    }

    @Override
    public void decoratorOperation() {
        decorator.decoratorOperation();
        System.out.println("Ship decorator performs operation");
    }
}
