package com.vrapalis.www.dp.sp.composite;

/**
 * Participant => Component
 */
public interface IFileComponent {

    String display();
    int getSize();
}
