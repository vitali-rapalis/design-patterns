package com.vrapalis.www.dp.bp.strategy;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class SendSubject {
    protected String participant;
    protected String title;
    protected String text;
}
