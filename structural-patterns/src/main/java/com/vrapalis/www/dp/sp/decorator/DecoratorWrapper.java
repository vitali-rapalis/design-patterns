package com.vrapalis.www.dp.sp.decorator;

public abstract class DecoratorWrapper implements IDecorator {
    IDecorator decorator;

    protected DecoratorWrapper(IDecorator decorator){
        this.decorator = decorator;
    }
}
