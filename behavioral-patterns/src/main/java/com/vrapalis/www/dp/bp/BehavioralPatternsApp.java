package com.vrapalis.www.dp.bp;

import com.vrapalis.www.dp.bp.strategy.Email;
import com.vrapalis.www.dp.bp.strategy.Notification;
import com.vrapalis.www.dp.bp.strategy.SendProcessor;
import com.vrapalis.www.dp.bp.strategy.Sms;
import lombok.val;

import java.util.List;

public class BehavioralPatternsApp {

    public static void main(String[] args) {
        /* Strategy Design Pattern */
        val sendProcessor = new SendProcessor();

        val email = new Email("maik@mail.com", "Login", "Email text ...");
        val sms = new Sms("014923409832", "Login", "Sms text ...");
        val notification = new Notification("device-nr-235345345", "Login", "Notification text ...");

        System.out.println("Process email");
        sendProcessor.process(email);
        System.out.println("Process email, sms, notification");
        sendProcessor.process(List.of(email, sms, notification));
    }
}
