package com.vrapalis.www.dp.sp.composite;


/**
 * Participant => Leaf
 */
public class FileLeaf extends AbstractFile {

    public FileLeaf(String name, int size) {
        super(name, size);
    }

    @Override
    public String display() {
        return """
                Name: %s Size: %d                
                """.formatted(name, fileSize);
    }

    @Override
    public int getSize() {
        return fileSize;
    }
}
