package com.vrapalis.www.dp.bp.strategy;

public class NotificationService implements ISendService {

    @Override
    public void send(SendSubject sendSubject) {
        if(!support(sendSubject.getClass())) {
            throw new UnsupportedOperationException("Only notification subjects are allowed");
        }

        print("Notification", sendSubject);
    }

    @Override
    public boolean support(Class<? extends SendSubject> cl) {
        return cl == Notification.class;
    }
}
