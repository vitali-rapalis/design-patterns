package com.vrapalis.www.dp.sp;

import com.vrapalis.www.dp.sp.composite.DirectoryComposite;
import com.vrapalis.www.dp.sp.composite.FileLeaf;
import com.vrapalis.www.dp.sp.decorator.IDecorator;
import com.vrapalis.www.dp.sp.decorator.OrderDecorator;
import com.vrapalis.www.dp.sp.decorator.Product;
import com.vrapalis.www.dp.sp.decorator.ShipDecoration;
import lombok.val;

import java.util.List;

public class StructuralPatternsApp {
    public static void main(String[] args) throws Exception {
        /* Composite Design Pattern Example */
        val rootDir = new DirectoryComposite("RootDir", 1, List.of(
                new FileLeaf("File1", 3),
                new FileLeaf("File2", 4),
                new FileLeaf("File3", 5),
                new DirectoryComposite("Dir1", 1, List.of(
                        new FileLeaf("File1", 3),
                        new FileLeaf("File2", 4),
                        new DirectoryComposite("Dir1/1", 1, List.of(
                                new DirectoryComposite("Dir1/2", 1, List.of(
                                        new FileLeaf("File1", 5)
                                ))
                        ))
                ))
        ));
        System.out.println("Composite design pattern example:");
        System.out.println(rootDir.display());
        System.out.print("Size of all files: " + rootDir.getSize());

        /* --- */

        /* Decorator Design Pattern */
        System.out.println("""
                Decorator Design pattern
                ---
                """);
        IDecorator decorator = new ShipDecoration(new OrderDecorator(new Product()));
        decorator.decoratorOperation();

        /* --- */
    }
}
