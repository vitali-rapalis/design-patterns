# Behavioral Patterns

---

> **Behavioral patterns** focus on the interaction and communication between objects, defining the patterns of communication 
> and responsibilities. They help in designing the interaction and coordination 
> between objects to achieve specific behaviors. Some common behavioral patterns include: