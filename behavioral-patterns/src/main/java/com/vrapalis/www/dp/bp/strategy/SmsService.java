package com.vrapalis.www.dp.bp.strategy;

public class SmsService implements ISendService {

    @Override
    public void send(SendSubject sendSubject) {
        if (!support(sendSubject.getClass())) {
            throw new UnsupportedOperationException("Only sms subjects are allowed");
        }

        print("Sms", sendSubject);
    }

    @Override
    public boolean support(Class<? extends SendSubject> cl) {
        return cl == Sms.class;
    }
}
