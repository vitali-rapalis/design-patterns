package com.vrapalis.www.dp.sp.decorator;

public class Product implements IDecorator {

    @Override
    public void decoratorOperation() {
        System.out.println("Product performing operation");
    }
}
