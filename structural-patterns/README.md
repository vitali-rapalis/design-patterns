# Structural Patterns

---

> **Structural patterns** focus on the composition of classes and objects to form
> larger structures while keeping the system flexible and efficient.
> They deal with the relationships between objects, enabling them to work together effectively.
> Some common structural patterns include:

## Patterns

- **Composite:** Composes objects into tree structures to represent part-whole hierarchies, allowing clients to treat
  individual objects and compositions uniformly. The pattern enables you to work with complex hierarchical structures
  as if they were individual objects. Example implementation can be found in package [*composite*](src/main/java/com/vrapalis/www/dp/sp/composite).
    - **Participants:**
        - *Component:* This is the base interface or abstract class that defines common operations for both the
          composite
          and leaf objects. It declares methods for accessing and manipulating child components, as well as for
          performing other common operations.
        - *Leaf:* The leaf class represents the individual objects in the composition. It doesn't have any child
          components. Leaf objects implement the operations defined by the Component interface.
        - *Composite:* The composite class represents the container or composite objects that can contain child
          components. It implements the operations declared by the Component interface, such as adding, removing, and
          accessing child components. A composite object usually delegates the actual work to its child components
          recursively.
    - **Structure:**
        - The Composite pattern forms a tree-like structure where both the composite and leaf objects share a common
          interface (Component). Each component, whether it's a leaf or a composite, can have child components. This
          recursive structure allows you to represent complex hierarchies.
    - **Usage:**
        - The Composite pattern is suitable when you want to represent part-whole hierarchies of objects.
        - It simplifies the client code by allowing the client to treat individual objects and compositions uniformly.
          The client doesn't need to distinguish between leaf and composite objects when working with them.
        - It provides a consistent way to work with both individual objects and groups of objects. Clients can apply
          operations on a single object or propagate them through the entire hierarchy.
        - The pattern supports the addition and removal of objects dynamically. You can add and remove leaf and
          composite objects without affecting the client code.
    - **Example:** <br/>
      Let's consider an example of a file system hierarchy. The Component interface would define common operations like
      displayName() or getSize(). The Leaf objects would represent individual files, while the Composite objects would
      represent directories. Directories can contain files (leaf objects) and other directories (composite objects). The
      client code can work with both files and directories uniformly, whether it's displaying the size of a single file
      or calculating the total size of a directory and its subdirectories.