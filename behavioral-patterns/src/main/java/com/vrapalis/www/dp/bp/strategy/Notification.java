package com.vrapalis.www.dp.bp.strategy;

public class Notification extends SendSubject {
    public Notification(String participant, String title, String text) {
        super(participant, title, text);
    }
}
